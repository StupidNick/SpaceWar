#include "SpaceWarGameModeBase.h"

#include "SavePlayer.h"
#include "Kismet/GameplayStatics.h"
#include "Components/HealthComponent.h"



ASpaceWarGameModeBase::ASpaceWarGameModeBase():
PlayerRecoverTime(3)
{
	EnemySpawnController = CreateDefaultSubobject<UEnemySpawnController>(TEXT("EnemySpawnController"));

	HealthComponents = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));

	BonusSpawnComponent = CreateDefaultSubobject<UBonusSpawnComponent>(TEXT("BonusSpawnComponent"));
}


void ASpaceWarGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	PlayerPawn = Cast<ASpaceShip>( UGameplayStatics::GetPlayerPawn(this, 0));

	if(MicroRocketCounter == 0)
		MicroRocketCounter = 3;

	if(ImmortalRecoverTime == 0)
		ImmortalRecoverTime = 5;

	if(BerserkRecoverTime == 0)
		BerserkRecoverTime = 5;

	if(RageRecoverTime == 0)
		RageRecoverTime = 5;

	if(CostBerserk == 0)
		CostBerserk = 5;

	if(CostImmortal == 0)
		CostImmortal = 5;

	if(CostRage == 0)
		CostRage = 5;

	if(CostMicroRocket == 0)
		CostMicroRocket = 5;
}


void ASpaceWarGameModeBase::SaveGame()
{
	if(USavePlayer *SaveGame = Cast<USavePlayer>(UGameplayStatics::CreateSaveGameObject(USavePlayer::StaticClass())))
	{
		SaveGame->Coins = Coins;
		SaveGame->HomingRocket = MicroRocketCounter;
		SaveGame->ImmortalTimer = ImmortalRecoverTime;
		SaveGame->BerserkTimer = BerserkRecoverTime;
		SaveGame->RageTimer = RageRecoverTime;
		SaveGame->CostRage = CostRage;
		SaveGame->CostMicroRocket = CostMicroRocket;
		SaveGame->CostImmortal = CostImmortal;
		SaveGame->CostBerserk = CostBerserk;

		UGameplayStatics::SaveGameToSlot(SaveGame, TEXT("MySlot"), 0);
    }
}


void ASpaceWarGameModeBase::LoadGame()
{
	if(USavePlayer *SaveGame = Cast<USavePlayer>(UGameplayStatics::CreateSaveGameObject(USavePlayer::StaticClass())))
	{
		SaveGame = Cast<USavePlayer>(UGameplayStatics::LoadGameFromSlot("MySlot", 0));

		MicroRocketCounter = SaveGame->HomingRocket;
		ImmortalRecoverTime = SaveGame->ImmortalTimer;
		BerserkRecoverTime = SaveGame->BerserkTimer;
		RageRecoverTime = SaveGame->RageTimer;
		CostRage = SaveGame->CostRage;
		CostMicroRocket = SaveGame->CostMicroRocket;
		CostImmortal = SaveGame->CostImmortal;
		CostBerserk = SaveGame->CostBerserk;
		Coins = SaveGame->Coins;
	}
}


void ASpaceWarGameModeBase::ExplodePawn_Implementation()
{
	PlayerPawn->BlowUpPawn();

	if(!IsGameOver)
		GetWorld()->GetTimerManager().SetTimer(RecoverTimer, this, &ASpaceWarGameModeBase::RecoverPawn, PlayerRecoverTime, false);
}


void ASpaceWarGameModeBase::RecoverPawn_Implementation()
{
	PlayerPawn->RecoverPawn();
}


void ASpaceWarGameModeBase::EndGame()
{
	IsGameOver = true;

	EnemySpawnController->SetActive(false);

	BonusSpawnComponent->SetActive(false);
	
	GameOver.Broadcast();

	Coins += CoinCounter;
	
	SaveGame();

	UGameplayStatics::GetPlayerPawn(this, 0)->Destroy();

	SetPause(UGameplayStatics::GetPlayerController(this, 0), false);
}


void ASpaceWarGameModeBase::AddPoints(int Points)
{
	GamePoints += Points;
}


void ASpaceWarGameModeBase::AddCoins(int Coin)
{
	CoinCounter += Coin;
}





//Deactivation of bonuses
void ASpaceWarGameModeBase::StopBerserkBonus()
{
	PlayerPawn->ShootComponent->StopShooting();
	PlayerPawn->ShootComponent->ShootPeriod *= 2;
	PlayerPawn->ShootComponent->StartShooting();
}


void ASpaceWarGameModeBase::StopRageBonus()
{
	for(int i = 0; i < PlayerPawn->ShootComponent->ShootInfos.Num(); i++)
	{
		PlayerPawn->ShootComponent->ShootInfos[i].Damage /= 2;
	}
}






//Bonus timers
void ASpaceWarGameModeBase::RecoverImmortalBonus()
{
	GetWorld()->GetTimerManager().SetTimer(RecoverBonusTimer, this, &ASpaceWarGameModeBase::RecoverPawn, ImmortalRecoverTime, false);
}


void ASpaceWarGameModeBase::RecoverBerserkBonus()
{
	GetWorld()->GetTimerManager().SetTimer(RecoverBonusTimer, this, &ASpaceWarGameModeBase::StopBerserkBonus, BerserkRecoverTime, false);
}


void ASpaceWarGameModeBase::RecoverRageBonus()
{
	GetWorld()->GetTimerManager().SetTimer(RecoverBonusTimer, this, &ASpaceWarGameModeBase::StopRageBonus, RageRecoverTime, false);
}