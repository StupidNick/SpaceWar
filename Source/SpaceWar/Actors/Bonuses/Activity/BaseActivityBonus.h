#pragma once

#include "CoreMinimal.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "SpaceWar/Pawns/SpaceShip.h"

#include "BaseActivityBonus.generated.h"



UCLASS()
class SPACEWAR_API ABaseActivityBonus : public ABaseBonus
{
	GENERATED_BODY()

protected:

	virtual void SetBonus() override;
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D *Texture;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ABaseActivityActor> Actor;
};
