#include "BaseActivityBonus.h"


#include "Kismet/GameplayStatics.h"
#include "SpaceWar/Pawns/SpaceShip.h"



void ABaseActivityBonus::SetBonus()
{
	APawn  *Pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	ASpaceShip *PlayerPawn = Cast<ASpaceShip>(Pawn);
	PlayerPawn->BonusComponent->ClassOfBonus = Actor;
	PlayerPawn->BonusComponent->Texture = Texture;
	PlayerPawn->BonusComponent->CanShoot = true;
}


