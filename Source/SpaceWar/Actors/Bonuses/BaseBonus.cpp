#include "BaseBonus.h"

#include "Activity/BaseActivityBonus.h"
#include "Kismet/GameplayStatics.h"


ABaseBonus::ABaseBonus():
SpeedMove(-100)
{
	PrimaryActorTick.bCanEverTick = true;
	
	PawnCollision = CreateDefaultSubobject<USphereComponent>("PawnCollision");
	RootComponent = PawnCollision;
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(RootComponent);
}


void ABaseBonus::BeginPlay()
{
	Super::BeginPlay();
	OnActorBeginOverlap.AddDynamic(this, &ABaseBonus::OnOverlap);
}


void ABaseBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalOffset(FVector(SpeedMove * DeltaTime, 0.f, 0.f));
}


void ABaseBonus::OnOverlap(AActor *OverlappedActor, AActor *OtherActor)//второй параметр - это игрок
{
	if(OtherActor != UGameplayStatics::GetPlayerPawn(this, 0))
		return;
	SetBonus();
	Destroy();
}

void ABaseBonus::SetBonus()
{}

