#pragma once

#include "CoreMinimal.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "SpaceWar/Actors/Projectiles/PlayersProjectile.h"

#include "BaseWeaponBonus.generated.h"



UCLASS()
class SPACEWAR_API ABaseWeaponBonus : public ABaseBonus
{
	GENERATED_BODY()

	virtual void SetBonus() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponInfo")
	TSubclassOf<APlayersProjectile> Projectile;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponInfo")
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponInfo")
	FVector Offset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponInfo")
	float Angle;
};
