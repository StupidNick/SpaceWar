#include "BaseWeaponBonus.h"

#include "Kismet/GameplayStatics.h"
#include "SpaceWar/SpaceWarGameModeBase.h"
#include "SpaceWar/Pawns/SpaceShip.h"


void ABaseWeaponBonus::SetBonus()
{
	APawn *Pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	ASpaceShip *PlayerPawn = Cast<ASpaceShip>(Pawn);

	FShootInfo Weapon;
	Weapon.Angle = Angle;
	Weapon.Damage = Damage;
	Weapon.Offset = Offset;
	Weapon.ProjectileClass = Projectile;
	PlayerPawn->ShootComponent->ShootInfos.Add(Weapon);
}
