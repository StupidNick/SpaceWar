#pragma once

#include "CoreMinimal.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "BerserkBonus.generated.h"


UCLASS()
class SPACEWAR_API ABerserkBonus : public ABaseBonus
{
	GENERATED_BODY()

	virtual void SetBonus() override;
};
