#pragma once

#include "CoreMinimal.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "RageBonus.generated.h"


UCLASS()
class SPACEWAR_API ARageBonus : public ABaseBonus
{
	GENERATED_BODY()

public:
	virtual void SetBonus() override;
};
