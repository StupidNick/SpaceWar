#include "ImmortalBonus.h"

#include "Kismet/GameplayStatics.h"
#include "SpaceWar/SpaceWarGameModeBase.h"
#include "SpaceWar/Pawns/SpaceShip.h"


AImmortalBonus::AImmortalBonus()
{}

void AImmortalBonus::SetBonus()
{
	APawn *Pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	ASpaceShip *PlayerPawn = Cast<ASpaceShip>(Pawn);

	PlayerPawn->SetActorEnableCollision(false);
	Cast<ASpaceWarGameModeBase>(UGameplayStatics::GetGameMode(this))->RecoverImmortalBonus();
}

