#include "RageBonus.h"


#include "Kismet/GameplayStatics.h"
#include "SpaceWar/SpaceWarGameModeBase.h"
#include "SpaceWar/Pawns/SpaceShip.h"

void ARageBonus::SetBonus()
{
	APawn *Pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	ASpaceShip *PlayerPawn = Cast<ASpaceShip>(Pawn);

	for(int i = 0; i < PlayerPawn->ShootComponent->ShootInfos.Num(); i++)
	{
		PlayerPawn->ShootComponent->ShootInfos[i].Damage *= 2;
	}
	Cast<ASpaceWarGameModeBase>(UGameplayStatics::GetGameMode(this))->RecoverRageBonus();
}
