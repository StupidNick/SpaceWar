#include "SpaceWar/Actors/Bonuses/Passive/BerserkBonus.h"


#include "Kismet/GameplayStatics.h"
#include "SpaceWar/SpaceWarGameModeBase.h"
#include "SpaceWar/Pawns/SpaceShip.h"


void ABerserkBonus::SetBonus()
{
	APawn *Pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	ASpaceShip *PlayerPawn = Cast<ASpaceShip>(Pawn);

	PlayerPawn->ShootComponent->StopShooting();
	PlayerPawn->ShootComponent->ShootPeriod /= 2;
	PlayerPawn->ShootComponent->StartShooting();
	Cast<ASpaceWarGameModeBase>(UGameplayStatics::GetGameMode(this))->RecoverBerserkBonus();
}
