#include "AttemptBonus.h"

#include "Kismet/GameplayStatics.h"
#include "SpaceWar/Pawns/SpaceShip.h"


void AAttemptBonus::SetBonus()
{
	APawn *Pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	ASpaceShip *PlayerPawn = Cast<ASpaceShip>(Pawn);
	
	PlayerPawn->HealthComponent->ChangeAttempt(1);
}
