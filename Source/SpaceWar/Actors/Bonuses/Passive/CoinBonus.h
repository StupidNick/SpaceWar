#pragma once

#include "CoreMinimal.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "CoinBonus.generated.h"


UCLASS()
class SPACEWAR_API ACoinBonus : public ABaseBonus
{
	GENERATED_BODY()

	virtual void SetBonus() override;
};
