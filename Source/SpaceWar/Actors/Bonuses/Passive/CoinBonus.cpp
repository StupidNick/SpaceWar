#include "CoinBonus.h"

#include "Kismet/GameplayStatics.h"
#include "SpaceWar/SpaceWarGameModeBase.h"



void ACoinBonus::SetBonus()
{
	Cast<ASpaceWarGameModeBase>(UGameplayStatics::GetGameMode(this))->AddCoins(1);
}
