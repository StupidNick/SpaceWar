#pragma once

#include "CoreMinimal.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "AttemptBonus.generated.h"



UCLASS()
class SPACEWAR_API AAttemptBonus : public ABaseBonus
{
	GENERATED_BODY()

protected:
	virtual void SetBonus() override;
};
	