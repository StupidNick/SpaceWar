#pragma once

#include "CoreMinimal.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "ImmortalBonus.generated.h"



UCLASS()
class SPACEWAR_API AImmortalBonus : public ABaseBonus
{
	GENERATED_BODY()

public:
	AImmortalBonus();

protected:
	virtual void SetBonus() override;
};
	