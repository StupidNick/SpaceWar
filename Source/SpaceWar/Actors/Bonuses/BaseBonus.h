#pragma once

#include "CoreMinimal.h"


#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "BaseBonus.generated.h"



UCLASS()
class SPACEWAR_API ABaseBonus : public AActor
{
	GENERATED_BODY()
	
public:	
	ABaseBonus();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOverlap(AActor *OverlappedActor, AActor *OtherActor);

	UFUNCTION()
	virtual void SetBonus();

public:
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pawn")
	UStaticMeshComponent *Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Pawn") 
	USphereComponent *PawnCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pawn") 
	float SpeedMove;
};
