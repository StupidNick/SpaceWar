#include "HomingRocket.h"



AHomingRocket::AHomingRocket():
Speed(5)
{
	PrimaryActorTick.bCanEverTick = true;
	
	Collision = CreateDefaultSubobject<UBoxComponent>("PawnCollision");
	RootComponent = Collision;
	Collision->SetCollisionResponseToAllChannels(ECR_Ignore);//Проджектайл игнорирует все
	Collision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);//Но с павном пересекается
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(Collision);
	Mesh->SetCollisionProfileName("NoCollision");
}


void AHomingRocket::BeginPlay()
{
	Super::BeginPlay();
	
	if (GetOwner())
	{
		Collision->IgnoreActorWhenMoving(GetOwner(), true);
	}
}


void AHomingRocket::Tick(float DeltaTime)
{
	AddActorLocalOffset(FVector(DeltaTime * Speed, 0.f, 0.f));
}
