#pragma once

#include "CoreMinimal.h"

#include "Components/SphereComponent.h"
#include "SpaceWar/Actors/Bonuses/Actors/BaseActivityActor.h"
#include "ElectromagnetImpulse.generated.h"



UCLASS()
class SPACEWAR_API AElectromagnetImpulse : public ABaseActivityActor
{
	GENERATED_BODY()

	AElectromagnetImpulse();
	
protected:
	virtual void BeginPlay() override;
	
	UFUNCTION()
    void OnOverlap(AActor *OverlappedActor, AActor *OtherActor);
	
public:
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	USphereComponent *Collision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Shooting")
	UStaticMeshComponent *Mesh;

	float Radius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Shooting")
	float ScallingSpeed;
};
