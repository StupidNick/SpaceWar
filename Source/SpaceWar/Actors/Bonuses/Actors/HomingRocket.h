#pragma once

#include "CoreMinimal.h"

#include "Components/BoxComponent.h"
#include "SpaceWar/Actors/Bonuses/Actors/BaseActivityActor.h"
#include "HomingRocket.generated.h"



UCLASS()
class SPACEWAR_API AHomingRocket : public ABaseActivityActor
{
	GENERATED_BODY()

	AHomingRocket();
	
	protected:
	virtual void BeginPlay() override;
	
	public:
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	UBoxComponent *Collision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Shooting")
	UStaticMeshComponent *Mesh;

	float Speed;
};
