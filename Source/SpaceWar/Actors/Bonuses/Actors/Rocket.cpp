#include "SpaceWar/Actors/Bonuses/Actors/Rocket.h"

#include "Kismet/GameplayStatics.h"


ARocket::ARocket():
Speed(2000.f)
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<UBoxComponent>("PawnCollision");
	RootComponent = Collision;
	Collision->SetCollisionResponseToAllChannels(ECR_Ignore);//Проджектайл игнорирует все
	Collision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);//Но с павном пересекается
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(Collision);
	Mesh->SetCollisionProfileName("NoCollision");
}


void ARocket::BeginPlay()
{
	Super::BeginPlay();
	
	if (GetOwner())
	{
		Collision->IgnoreActorWhenMoving(GetOwner(), true);
	}
	
	OnActorBeginOverlap.AddDynamic(this, &ARocket::OnOverlap);
}


void ARocket::Tick(float DeltaTime)
{
	AddActorLocalOffset(FVector(DeltaTime * Speed, 0.f, 0.f));
}


void ARocket::OnOverlap(AActor *OverlappedActor, AActor *OtherActor)//второй параметр - это игрок
{
	if(!OtherActor || !Cast<APawn>(OtherActor))//Если проджектайл не пересекается с эктором или пересекается не с павном
		return;
	if(OtherActor == GetOwner())
		return;
	APawn *PawnOwner = Cast<APawn>(GetOwner());
	
	AController *Instigators = PawnOwner->GetController();
	UGameplayStatics::ApplyDamage(OtherActor, Damage, Instigators, this, UDamageType::StaticClass());
	
	Destroy();
}
