#include "ElectromagnetImpulse.h"

#include "SpaceWar/Pawns/EnemyPawn.h"


AElectromagnetImpulse::AElectromagnetImpulse():
Radius(5),
ScallingSpeed(1000)
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>("PawnCollision");
	RootComponent = Collision;
	Collision->SetCollisionResponseToAllChannels(ECR_Ignore);//Проджектайл игнорирует все
	Collision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);//Но с павном пересекается
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(Collision);
	Mesh->SetCollisionProfileName("NoCollision");
}

void AElectromagnetImpulse::BeginPlay()
{
	Super::BeginPlay();
	
	if (GetOwner())
	{
		Collision->IgnoreActorWhenMoving(GetOwner(), true);
	}
	
	OnActorBeginOverlap.AddDynamic(this, &AElectromagnetImpulse::OnOverlap);
}


void AElectromagnetImpulse::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if(!OtherActor || !Cast<APawn>(OtherActor))//Если проджектайл не пересекается с эктором или пересекается не с павном
		return;
	if(OtherActor == GetOwner())
		return;

	Cast<AEnemyPawn>(OtherActor)->ShootComponent->StopShooting();
}


void AElectromagnetImpulse::Tick(float DeltaTime)
{
	if(Radius <= 6000)
	{
		Collision->SetSphereRadius(Radius);
		Mesh->SetRelativeScale3D(FVector(Radius/45,Radius/45,0.f));
		Radius += DeltaTime * ScallingSpeed;
	}
	else
		Destroy();
}
