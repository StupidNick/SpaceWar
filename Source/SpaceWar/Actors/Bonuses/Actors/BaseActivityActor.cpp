#include "BaseActivityActor.h"


ABaseActivityActor::ABaseActivityActor()
{
	PrimaryActorTick.bCanEverTick = true;

}


void ABaseActivityActor::BeginPlay()
{
	Super::BeginPlay();
	
}


void ABaseActivityActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

