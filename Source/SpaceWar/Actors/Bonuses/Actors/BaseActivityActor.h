#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"


#include "BaseActivityActor.generated.h"

UCLASS()
class SPACEWAR_API ABaseActivityActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ABaseActivityActor();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
};
