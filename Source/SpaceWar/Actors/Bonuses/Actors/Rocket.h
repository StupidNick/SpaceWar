#pragma once

#include "CoreMinimal.h"

#include "Components/BoxComponent.h"
#include "SpaceWar/Actors/Bonuses/Actors/BaseActivityActor.h"
#include "Rocket.generated.h"



UCLASS()
class SPACEWAR_API ARocket : public ABaseActivityActor
{
	GENERATED_BODY()
	
	ARocket();

protected:
	virtual void BeginPlay() override;
	
	UFUNCTION()
	void OnOverlap(AActor *OverlappedActor, AActor *OtherActor);
	
public:
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	UBoxComponent *Collision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	UStaticMeshComponent *Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Info")
	float Speed;
};
