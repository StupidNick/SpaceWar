#include "MicroRocket.h"


AMicroRocket::AMicroRocket()
{
	PrimaryActorTick.bCanEverTick = true;
	
	Collision = CreateDefaultSubobject<UBoxComponent>("PawnCollision");
	RootComponent = Collision;
	Collision->SetCollisionResponseToAllChannels(ECR_Ignore);//Проджектайл игнорирует все
	Collision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);//Но с павном пересекается
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(Collision);
	Mesh->SetCollisionProfileName("NoCollision");
}


void AMicroRocket::BeginPlay()
{
	Super::BeginPlay();
	
	if (GetOwner())
	{
		Collision->IgnoreActorWhenMoving(GetOwner(), true);
	}
}


void AMicroRocket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}