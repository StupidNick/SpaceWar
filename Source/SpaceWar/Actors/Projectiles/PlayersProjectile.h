#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "PlayersProjectile.generated.h"

/**
 * 
 */
UCLASS()
class SPACEWAR_API APlayersProjectile : public AProjectile
{
	GENERATED_BODY()
public:
	

protected:
	virtual void OnProjectileOverlap(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 BodyIndex, bool Sweep, const FHitResult &Hit) override;

};
