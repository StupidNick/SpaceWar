#include "PlayersProjectile.h"

#include "Kismet/GameplayStatics.h"

void APlayersProjectile::OnProjectileOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
                                             UPrimitiveComponent* OtherComp, int32 BodyIndex, bool Sweep, const FHitResult& Hit)
{
	
	if(!OtherActor || !Cast<APawn>(OtherActor))//Если проджектайл не пересекается с эктором или пересекается не с павном
		return;
	if(OtherActor == GetOwner())
		return;
	APawn *PawnOwner = Cast<APawn>(GetOwner());
	
	AController *Instigators = PawnOwner->GetController();
	UGameplayStatics::ApplyDamage(OtherActor, Damage, Instigators, this, UDamageType::StaticClass());
	Destroy();
}
