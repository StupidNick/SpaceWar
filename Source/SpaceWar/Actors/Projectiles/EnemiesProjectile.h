#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "EnemiesProjectile.generated.h"

/**
 * 
 */
UCLASS()
class SPACEWAR_API AEnemiesProjectile : public AProjectile
{
	GENERATED_BODY()

protected:
	virtual void OnProjectileOverlap(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 BodyIndex, bool Sweep, const FHitResult &Hit) override;
	
};
