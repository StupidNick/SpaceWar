#include "EnemiesProjectile.h"

#include "Kismet/GameplayStatics.h"
#include "SpaceWar/Pawns/SpaceShip.h"


void AEnemiesProjectile::OnProjectileOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
                                             UPrimitiveComponent* OtherComp, int32 BodyIndex, bool Sweep, const FHitResult& Hit)
{
	if(OtherActor == GetOwner())
		return;
	if(!OtherActor || !Cast<ASpaceShip>(OtherActor))//Если проджектайл не пересекается с эктором или пересекается не с павном
		return;
	if (Cast<ASpaceShip>(OtherActor))
	{
		APawn *PawnOwner = Cast<APawn>(GetOwner());
	
		AController *Instigators = PawnOwner->GetController();
		UGameplayStatics::ApplyDamage(OtherActor, Damage, Instigators, this, UDamageType::StaticClass());

		Destroy();
	}
}
