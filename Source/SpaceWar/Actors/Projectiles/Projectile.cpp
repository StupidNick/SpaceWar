#include "Projectile.h"
#include "GameFramework/DamageType.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"


AProjectile::AProjectile()
	: ProjectileSpeed(3000.f)
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>("PawnCollision");
	RootComponent = Collision;
	Collision->SetCollisionResponseToAllChannels(ECR_Ignore);//Проджектайл игнорирует все
	Collision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);//Но с павном пересекается
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(Collision);
	Mesh->SetCollisionProfileName("NoCollision");
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		Collision->IgnoreActorWhenMoving(GetOwner(), true);
	}
	
	Collision->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnProjectileOverlap);
}


void AProjectile::OnProjectileOverlap(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 BodyIndex, bool Sweep, const FHitResult &Hit)
{

}


void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalOffset(FVector(ProjectileSpeed * DeltaTime, 0.f, 0.f));
}

