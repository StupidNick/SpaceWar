// Fill out your copyright notice in the Description page of Project Settings.


#include "PlaygroundBorder.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
APlaygroundBorder::APlaygroundBorder()
{
	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	SetRootComponent(Trigger);

	Trigger->SetCollisionProfileName("OverlapAll");
}

void APlaygroundBorder::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	if(OtherActor != UGameplayStatics::GetPlayerPawn(this, 0))
		OtherActor->Destroy();
}
