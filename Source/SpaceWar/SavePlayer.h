#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SavePlayer.generated.h"



UCLASS()
class SPACEWAR_API USavePlayer : public USaveGame
{
	GENERATED_BODY()

	USavePlayer();

public:
	UPROPERTY(VisibleAnywhere, Category = "Basic")
	int Coins;
	
	UPROPERTY(VisibleAnywhere, Category = "Basic")
	int HomingRocket;

	UPROPERTY(VisibleAnywhere, Category = "Basic")
	float BerserkTimer;

	UPROPERTY(VisibleAnywhere, Category = "Basic")
	float ImmortalTimer;

	UPROPERTY(VisibleAnywhere, Category = "Basic")
	float RageTimer;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Bonus")
	int CostRage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Bonus")
	int CostImmortal;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Bonus")
	int CostBerserk;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Bonus")
	int CostMicroRocket;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Game")\
	bool FirstStart;
};
