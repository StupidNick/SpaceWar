#include "SpaceShip.h"

#include "EnemyPawn.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"


ASpaceShip::ASpaceShip():
	Sensivity(1.f),
	MoveLimit(FVector2D(2400, 1500))
{
	PrimaryActorTick.bCanEverTick = true;

	PawnCollision = CreateDefaultSubobject<UBoxComponent>("PawnCollision");
	RootComponent = PawnCollision;
	PawnCollision->SetCollisionProfileName("Pawn");
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");

	ShootComponent = CreateDefaultSubobject<UShootComponent>("ShootComponent");
	
	HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");

	BonusComponent = CreateDefaultSubobject<UBonusComponent>("BonusComponent");
}


void ASpaceShip::BeginPlay()
{
	Super::BeginPlay();

	PawnMaterial = Mesh->GetMaterial(1);

	OnActorBeginOverlap.AddDynamic(this, &ASpaceShip::OnPlayerOverlap);

	PlayerController->SetShowMouseCursor(true);
}


void ASpaceShip::OnPlayerOverlap(AActor *OverlappedActor, AActor *OtherActor)//второй параметр - это игрок
{
	if(Cast<AEnemyPawn>(OtherActor))
		HealthComponent->DecreaseShield(100);
}


bool ASpaceShip::CanBeDamaged_Implementation()
{
	return CanBeDamaged();
}


void ASpaceShip::BlowUpPawn_Implementation()
{
	SetActorEnableCollision(false);

	ShootComponent->StopShooting();

	Mesh->SetMaterial(1, RecoverMaterial);
}


void ASpaceShip::RecoverPawn_Implementation()
{
	SetActorEnableCollision(true);

	ShootComponent->StartShooting();

	Mesh->SetMaterial(1, PawnMaterial);
}


void ASpaceShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if(HealthComponent->Timer)//Timer for recover shield
	{
		HealthComponent->ShieldRecoverTime -= DeltaTime;
		if(HealthComponent->ShieldRecoverTime <= 0)
		{
			HealthComponent->RecoverSield = true;
			HealthComponent->Timer = false;
		}
	}
	if(HealthComponent->RecoverSield == true)
		HealthComponent->RecoveryShield(DeltaTime * HealthComponent->SpeedRecovery);
}


void ASpaceShip::PossessedBy(AController *NewController)
{
	PlayerController = Cast<APlayerController>(NewController); //Создаем новый контроеллер и получаем текущий контроллер в виде APlayerController
}


void ASpaceShip::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindTouch(IE_Pressed, this, &ASpaceShip::OnTouchPress);

	InputComponent->BindTouch(IE_Repeat, this, &ASpaceShip::OnTouchMove);
}


void ASpaceShip::OnTouchMove(ETouchIndex::Type Index, FVector Location)
{
	FVector2D TouchDeltaMove = FVector2D(TouchLocation.X-Location.X, TouchLocation.Y-Location.Y);

	FVector NewLocation = GetActorLocation();
	NewLocation.X = FMath::Clamp(NewLocation.X + TouchDeltaMove.Y * Sensivity, -MoveLimit.Y, MoveLimit.Y);
	NewLocation.Y = FMath::Clamp(NewLocation.Y + TouchDeltaMove.X * -Sensivity, -MoveLimit.X, MoveLimit.X);

	SetActorLocation(NewLocation); // Смещаем наш Actor на значения по х и у

	TouchLocation = FVector2D(Location.X, Location.Y);
}


void ASpaceShip::OnTouchPress(ETouchIndex::Type Index, FVector Location)
{
	TouchLocation = FVector2D(Location.X, Location.Y);
}


