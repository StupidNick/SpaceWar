#pragma once

#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "SpaceWar/Components/BonusComponent.h"
#include "SpaceWar/Components/HealthComponent.h"
#include "SpaceWar/Components/ShootComponent.h"

#include "SpaceShip.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPawnDamagedEvent);



UCLASS()
class SPACEWAR_API ASpaceShip : public APawn
{
	GENERATED_BODY()

public:
	ASpaceShip();

private:
	FVector2D TouchLocation;

	bool Touching;

	UMaterialInterface *PawnMaterial;

protected:	
	virtual void BeginPlay() override;

	void OnTouchMove(ETouchIndex::Type Index, FVector Location);
	void OnTouchPress(ETouchIndex::Type Index, FVector Location);

	virtual void PossessedBy(AController *NewController) override;
	
	APlayerController *PlayerController;

	FVector2D MoveLimit;

public:	
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = "Health")
    bool CanBeDamaged();
	bool CanBeDamaged_Implementation();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Health")
    void BlowUpPawn();
	void BlowUpPawn_Implementation();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Health")
    void RecoverPawn();
	void RecoverPawn_Implementation();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pawn")
	UStaticMeshComponent *Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Pawn") 
	UBoxComponent *PawnCollision;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Pawn")
	UCameraComponent *Camera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	UShootComponent *ShootComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	UHealthComponent *HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	UBonusComponent *BonusComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Controls")
	float Sensivity;

	UPROPERTY(BlueprintAssignable, Category="Health")
	FPawnDamagedEvent PawnDamaged;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pawn")
	UMaterialInterface *RecoverMaterial;

	UFUNCTION()
	void OnPlayerOverlap(AActor *OverlappedActor, AActor *OtherActor);
};
