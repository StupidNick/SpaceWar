#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "SpaceWar/Components/ShootComponent.h"
#include "GameFramework/Pawn.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"
#include "SpaceWar/Components/BonusSpawnComponent.h"
#include "SpaceWar/Components/HealthComponent.h"

#include "EnemyPawn.generated.h"

UCLASS()
class SPACEWAR_API AEnemyPawn : public APawn
{
	GENERATED_BODY()

public:
	AEnemyPawn();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void DestroyPawn();
	
	UFUNCTION()
    void OnEnemyOverlap(UPrimitiveComponent* px, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherIndex, bool bFromSweep, const FHitResult & SweepResult);

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pawn")
	USceneComponent *RootSceneComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pawn")
	UStaticMeshComponent *Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Pawn") 
	UBoxComponent *PawnCollision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	UShootComponent *ShootComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	UHealthComponent *HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pawn")
	float SpeedPawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Player")
	float PlayersDamageOnOverlap;
};
