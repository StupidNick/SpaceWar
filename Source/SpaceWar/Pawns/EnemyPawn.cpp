#include "EnemyPawn.h"

#include "Kismet/GameplayStatics.h"
#include "SpaceWar/SpaceWarGameModeBase.h"
#include "SpaceWar/Components/HealthComponent.h"


AEnemyPawn::AEnemyPawn():
	SpeedPawn(200.f),
	PlayersDamageOnOverlap(100)
{
	PrimaryActorTick.bCanEverTick = true;

	RootSceneComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	RootComponent = RootSceneComponent;
	
	PawnCollision = CreateDefaultSubobject<UBoxComponent>("PawnCollision");
	PawnCollision->SetCollisionProfileName("Pawn");
	
	PawnCollision->SetupAttachment(RootComponent);
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	Mesh->SetupAttachment(PawnCollision);

	ShootComponent = CreateDefaultSubobject<UShootComponent>("ShootComponent");

	HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");
}


void AEnemyPawn::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->OnAllAttemptEnded.AddDynamic(this, &AEnemyPawn::DestroyPawn);
	PawnCollision->OnComponentBeginOverlap.AddDynamic(this, &AEnemyPawn::OnEnemyOverlap);
}


void AEnemyPawn::OnEnemyOverlap(UPrimitiveComponent* px, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherIndex, bool bFromSweep, const FHitResult & SweepResult)//второй параметр - это игрок
{
	if(OtherActor != UGameplayStatics::GetPlayerPawn(this, 0))
		return;
	HealthComponent->ChangeHealth(PlayersDamageOnOverlap);
}


void AEnemyPawn::DestroyPawn()
{
	Destroy();
}


void AEnemyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalOffset(FVector(0.f, SpeedPawn * DeltaTime, 0.f));
}


void AEnemyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

