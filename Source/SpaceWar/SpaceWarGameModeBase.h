#pragma once

#include "CoreMinimal.h"

#include "Components/BonusSpawnComponent.h"
#include "GameFramework/GameModeBase.h"
#include "Components/EnemySpawnController.h"
#include "Components/HealthComponent.h"

#include "SpaceWarGameModeBase.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameOverEvent);


UCLASS()
class SPACEWAR_API ASpaceWarGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	ASpaceWarGameModeBase();

	virtual void BeginPlay() override;
protected:
	FTimerHandle RecoverTimer;
	
	FTimerHandle RecoverBonusTimer;

	bool IsGameOver;



public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enemies");
	UEnemySpawnController *EnemySpawnController;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Health")
	UHealthComponent *HealthComponents;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Health")
	UBonusSpawnComponent *BonusSpawnComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Game")
	class ASpaceShip *PlayerPawn;
	
	UFUNCTION(BlueprintCallable, Category = "Game")
	void EndGame();

	UFUNCTION(BlueprintCallable, Category = "Bonus")
	void StopBerserkBonus();

	UFUNCTION(BlueprintCallable, Category = "Bonus")
	void StopRageBonus();

	UFUNCTION(BlueprintCallable, Category = "Bonus")
	void AddPoints(int Points);

	UFUNCTION(BlueprintCallable, Category = "Game")
	void AddCoins(int Coin);
	
	UPROPERTY(BlueprintAssignable, Category = "Game")
	FGameOverEvent GameOver;
	
	UPROPERTY(BlueprintReadWrite, Category = "Bonus")
    int CoinCounter;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Bonus")
	int MicroRocketCounter;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Bonus")
	int CostRage;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Bonus")
	int CostImmortal;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Bonus")
	int CostBerserk;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Bonus")
	int CostMicroRocket;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Bonus")
	int Coins;

	

	
	UPROPERTY(BlueprintReadWrite, Category = "Game")
	float PlayerRecoverTime;

	UPROPERTY(BlueprintReadWrite, Category = "Bonus")
	float ImmortalRecoverTime;

	UPROPERTY(BlueprintReadWrite, Category = "Bonus")
	float BerserkRecoverTime;

	UPROPERTY(BlueprintReadWrite, Category = "Bonus")
	float RageRecoverTime;

	
	


	UPROPERTY(BlueprintReadOnly, Category = "Game")
	int GamePoints;
	
	UFUNCTION(BlueprintNativeEvent, Category = "Game")
    void ExplodePawn();
	void ExplodePawn_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "Game")
    void RecoverPawn();
	void RecoverPawn_Implementation();



	
	UFUNCTION(BlueprintCallable, Category = "Bonus")
	void RecoverImmortalBonus();

	UFUNCTION(BlueprintCallable, Category = "Bonus")
	void RecoverBerserkBonus();

	UFUNCTION(BlueprintCallable, Category = "Bonus")
	void RecoverRageBonus();



	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void SaveGame();

	UFUNCTION(BlueprintCallable, Category = "SaveLoad")
	void LoadGame();
};
