#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SpaceWar/Actors/Bonuses/BaseBonus.h"


#include "BonusSpawnComponent.generated.h"



USTRUCT(BlueprintType)
struct FBonusSpawnInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Bonus")
	TSubclassOf<ABaseBonus> BonusClass = ABaseBonus::StaticClass();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
	FTransform SpawnTransform;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPACEWAR_API UBonusSpawnComponent : public UActorComponent
{
	GENERATED_BODY()

	UBonusSpawnComponent();

protected:
	virtual void BeginPlay() override;

	virtual void Deactivate() override;

	void SpawnBonus();
	void StartSpawnStage();

	FBonusSpawnInfo SpawnStage;

	int EnemiesSpawned;

	FTimerHandle ChangeStageTimer;
	FTimerHandle EnemySpawnTimer;

	FRandomStream Random;

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Enemies")
	TArray<FBonusSpawnInfo> SpawnStages;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Enemies")
	float StageMinDelay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Enemies")
	float StageMaxDelay;
};