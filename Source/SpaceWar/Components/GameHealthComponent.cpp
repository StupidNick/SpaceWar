
#include "GameHealthComponent.h"

#include "Kismet/GameplayStatics.h"


UGameHealthComponent::UGameHealthComponent():
Health(3)
{

}


void UGameHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	APawn *PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	if(!PlayerPawn)
	{
		UE_LOG(LogTemp, Error, TEXT("No Player Pawn!"));
		return;
	}
}


void UGameHealthComponent::ChangeHealth(int ByValue)//меняем жизни с помощью диспатчера, если их меньше нуля, вызываем диспатчер конца игры
{
	Health +=ByValue;

	HealthsChanged.Broadcast(ByValue);
	if(Health <= 0)
	{
		HealthsEnded.Broadcast();
	}

	UE_LOG(LogTemp, Log, TEXT("%i"), Health);

}


int UGameHealthComponent::GetHealth()
{
	return Health;
}
