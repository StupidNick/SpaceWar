#include "HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "SpaceWar/SpaceWarGameModeBase.h"


UHealthComponent::UHealthComponent():
	Health(100),
	Attempt(3),
	Shield(100),
	ShieldRecoverTime(3),
	SpeedRecovery(5)
{}


void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnOwnerDamaged);
}


void UHealthComponent::OnOwnerDamaged(AActor* DamagedActor, float Damage, const UDamageType *DamageType,
	AController* Instigator, AActor* DamageCauser)
{
	if(GetOwner() == UGameplayStatics::GetPlayerPawn(this, 0))
		DecreaseShield(Damage);
	else
		ChangeHealth(-Damage);
}


void UHealthComponent::ChangeHealth(float Value)
{
	Health += Value;
	if (Health <= 0.f)
	{
		GetOwner()->Destroy();
		Cast<ASpaceWarGameModeBase>(UGameplayStatics::GetGameMode(this))->AddPoints(Points);
	}
}


void UHealthComponent::ChangeHealthPlayer(float Value)
{
	Health += Value;
	if (Health <= 0.f)
		ChangeAttempt(-1);
}


void UHealthComponent::DecreaseShield(float Value)
{
	Timer = true;
	RecoverSield = false;
	ShieldRecoverTime = 3;
	Shield -= Value;
	if(Shield <= 0)
	{
		float HealthTime = Shield;
		Shield = 0;
		ChangeHealthPlayer(HealthTime);
	}
}


void UHealthComponent::RecoveryShield(float Value)
{
	Shield += Value;
	if(Shield >= 100)
	{
		Shield = 100;
		RecoverSield = false;
	}
}


void UHealthComponent::ChangeAttempt(int Value)
{
	Attempt += Value;
	Health = 100;
	Shield = 100;
	if(Attempt <= 0)
		Cast<ASpaceWarGameModeBase>(UGameplayStatics::GetGameMode(this))->EndGame();
	else
		if(Value < 0)
			Cast<ASpaceWarGameModeBase>(UGameplayStatics::GetGameMode(this))->ExplodePawn();
}



//Getters
int UHealthComponent::GetAttempt()
{
	return Attempt;
}


float UHealthComponent::GetHealth()
{
	return Health;
}


float UHealthComponent::GetShield()
{
	return Shield;
}