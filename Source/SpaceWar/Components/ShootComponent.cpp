#include "ShootComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"



UShootComponent::UShootComponent()
{}


void UShootComponent::BeginPlay()
{
	Super::BeginPlay();
	StartShooting();
}


void UShootComponent::Shoot()
{
	for(int i = 0; i < ShootInfos.Num(); i++)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = GetOwner();
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;//Заставляет спавниться экторы в любом случе, даже если они, например, в стене

		FVector SpawnLocation = GetOwner()->GetActorLocation() + GetOwner()->GetActorRotation().RotateVector(ShootInfos[i].Offset);

		FRotator SpawnRotation = GetOwner()->GetActorRotation();
		SpawnRotation.Add(0.f, ShootInfos[i].Angle, 0.f);//поворот снарядов

		AProjectile *Projectile = GetWorld()->SpawnActor<AProjectile>(ShootInfos[i].ProjectileClass, SpawnLocation, SpawnRotation, SpawnParameters);
		if(Projectile) 
			Projectile->Damage = ShootInfos[i].Damage;
	}
}


void UShootComponent::StartShooting()
{
	GetWorld()->GetTimerManager().SetTimer(ShootingTimer, this, &UShootComponent::Shoot, ShootPeriod, true, ShootPeriod);
}


void UShootComponent::StopShooting()
{
	GetWorld()->GetTimerManager().ClearTimer(ShootingTimer);
}

