#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "HealthComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthEndedEvent);//Объявление диспатчера без параметров, чтобы указать параметры, нужно написать в конце через подчеркивание их количество,
											 //например _OneParam или TwoParams

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPACEWAR_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthComponent();

protected:
	virtual void BeginPlay() override;

	

	float Shield;

	UFUNCTION()
	void OnOwnerDamaged(AActor *DamagedActor, float Damage, const UDamageType *DamageType, AController *Instigator, AActor *DamageCauser);

	UPROPERTY(EditAnywhere, Category = "Game Health")
	int Attempt;

public:
	UFUNCTION(BlueprintCallable, Category = "Health")
	void ChangeHealth(float Value);
	
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealth();

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetShield();

	UFUNCTION(BlueprintCallable, Category = "Health") 
	void ChangeHealthPlayer(float Value);

	UFUNCTION(BlueprintCallable, Category = "Health")
	void DecreaseShield(float Value);

	UFUNCTION(BlueprintCallable, Category = "Health")
	void RecoveryShield(float Value);

	UPROPERTY(BlueprintAssignable, Category = "Health")
	FHealthEndedEvent OnAllAttemptEnded;//объявление диспачера, теперь через переменную HealthEnded можно получить к нему доступ

	UFUNCTION(BlueprintCallable, Category = "Game Health")
    void ChangeAttempt(int Value);

	UFUNCTION(BlueprintPure, Category = "Game Health")
    int GetAttempt();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Health")
	int Points;

	float SpeedRecovery;

	bool RecoverSield;

	bool Timer;
	
	UPROPERTY(BlueprintReadWrite, Category = "Game")
	float ShieldRecoverTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Health")
	float Health;
};
