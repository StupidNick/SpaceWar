#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SpaceWar/Actors/Bonuses/Actors/BaseActivityActor.h"

#include "BonusComponent.generated.h"

class BaseActivityActor;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPACEWAR_API UBonusComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UBonusComponent();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool CanShoot;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
	TSubclassOf<ABaseActivityActor> ClassOfBonus;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D *Texture;
};
