#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SpaceWar/Pawns/EnemyPawn.h"
#include "SpaceWar/Pawns/SpaceShip.h"
#include "SpaceWar/Pawns/EnemyPawn.h"

#include "EnemySpawnController.generated.h"



USTRUCT(BlueprintType)
struct FEnemySpawnInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Enemies")
	TSubclassOf<AEnemyPawn> EnemyClass = AEnemyPawn::StaticClass();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemies")
	FTransform SpawnTransform;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemies")
	int NumOfEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemies")
	float SpawnDelay;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPACEWAR_API UEnemySpawnController : public UActorComponent
{
	GENERATED_BODY()

public:	
	UEnemySpawnController();

protected:
	virtual void BeginPlay() override;

	virtual void Deactivate() override;

	void SpawnEnemy();
	void StartSpawnStage();

	FEnemySpawnInfo SpawnStage;

	int EnemiesSpawned;

	FTimerHandle ChangeStageTimer;
	FTimerHandle EnemySpawnTimer;

	FRandomStream Random;

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Enemies")
	TArray<FEnemySpawnInfo> SpawnStages;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Enemies")
	float StageMinDelay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Enemies")
	float StageMaxDelay;


};
