#pragma once

#include "CoreMinimal.h"
#include "Chaos/AABBTree.h"
#include "Components/ActorComponent.h"
#include "SpaceWar/Actors/Projectiles/Projectile.h"

#include "ShootComponent.generated.h"


USTRUCT(BlueprintType)
struct FShootInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	TSubclassOf<AProjectile> ProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	FVector Offset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float Angle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float Damage;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPACEWAR_API UShootComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UShootComponent();

protected:
	virtual void BeginPlay() override;

	void Shoot();

public:

	UFUNCTION(BlueprintCallable, Category="Shooting")
	void StartShooting();

	UFUNCTION(BlueprintCallable, Category="Shooting")
    void StopShooting();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Controls")
	float ShootPeriod;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooting")
	TArray<FShootInfo> ShootInfos;

	FTimerHandle ShootingTimer;
};
