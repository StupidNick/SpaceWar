#include "BonusSpawnComponent.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "SpaceWar/Pawns/EnemyPawn.h"


UBonusSpawnComponent::UBonusSpawnComponent()
{
	
}


void UBonusSpawnComponent::BeginPlay()
{
	Super::BeginPlay();

	Random.GenerateNewSeed(); // Позволяет генерировать разное зерно при каждом новом запуске
	
	StartSpawnStage();
}


void UBonusSpawnComponent::Deactivate()
{	
	GetWorld()->GetTimerManager().ClearTimer(ChangeStageTimer);
	GetWorld()->GetTimerManager().ClearTimer(EnemySpawnTimer);
}


void UBonusSpawnComponent::StartSpawnStage()
{
	SpawnStage = SpawnStages[Random.RandRange(0, SpawnStages.Num() - 1)];
	
	EnemiesSpawned = 0;
	SpawnBonus();
	
	GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UBonusSpawnComponent::StartSpawnStage, Random.RandRange(StageMinDelay, StageMaxDelay), false);
}


void UBonusSpawnComponent::SpawnBonus()
{
	FActorSpawnParameters SpawnParameters;
	SpawnStage.SpawnTransform.SetLocation(FVector(2000.f, Random.RandRange(-2500, 2500), 0.f)); 
	GetWorld()->SpawnActor<ABaseBonus>(SpawnStage.BonusClass, SpawnStage.SpawnTransform, SpawnParameters);
}