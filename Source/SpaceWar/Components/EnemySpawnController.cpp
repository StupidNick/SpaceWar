#include "EnemySpawnController.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "SpaceWar/Pawns/EnemyPawn.h"


UEnemySpawnController::UEnemySpawnController()
{}


void UEnemySpawnController::BeginPlay()
{
	Super::BeginPlay();

	Random.GenerateNewSeed(); // Позволяет генерировать разное зерно при каждом новом запуске
	
	StartSpawnStage();
}


void UEnemySpawnController::Deactivate()
{	
	GetWorld()->GetTimerManager().ClearTimer(ChangeStageTimer);
	GetWorld()->GetTimerManager().ClearTimer(EnemySpawnTimer);
}


void UEnemySpawnController::StartSpawnStage()
{
	SpawnStage = SpawnStages[Random.RandRange(0, SpawnStages.Num() - 1)];
	
	EnemiesSpawned = 0;
	SpawnEnemy();
	
	GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, &UEnemySpawnController::StartSpawnStage, Random.RandRange(StageMinDelay, StageMaxDelay), false);
}


void UEnemySpawnController::SpawnEnemy()
{
	FActorSpawnParameters SpawnParameters;
	GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, SpawnStage.SpawnTransform, SpawnParameters);
	
	EnemiesSpawned++;
	if (EnemiesSpawned < SpawnStage.NumOfEnemies)
		GetWorld()->GetTimerManager().SetTimer(EnemySpawnTimer, this, &UEnemySpawnController::SpawnEnemy, SpawnStage.SpawnDelay, false);
}